import cityskyline from './city-skyline.jpg';
import cntower from './cn-tower.jpg';
import rogerscenter from './rogers-center.jpg';
import oldcityhall from './old-city-hall.jpg';
import rom1 from './rom.jpg';
import newcityhall from './new-city-hall.jpg';

import './App.css';
import { useState } from 'react';
//import 'https://www.w3schools.com/w3css/4/w3.css'

function App() {
  const images = [cityskyline, oldcityhall,cntower, rom1, rogerscenter, newcityhall];
  const [selectedImage, setSelectedImage] = useState(null);

  return (
    <div className="App">
      <header className="App-header">
      <div className="App-headertext">
      <p>The City of Toronto</p>
      <p>Historical Sites & More</p>
      </div>
      </header>
      <section class="grid">
        {
          images.map(image => <img src={image} alt="logo" onClick={()=>setSelectedImage(image)} />)
        }

      <div class="overlay">
       <div class="text">Hello World</div>
      </div>
      <div>
        <p>facebook Twitter</p>        
      </div>
      </section>
      <div id='overlay' style={{visibility: selectedImage ? 'visible': 'hidden'}}>
        <h1><a class="close" onClick={ ()=>setSelectedImage(null) }>X</a></h1>
        <img src={selectedImage} />
      </div>

    </div>
  );
}

export default App;
